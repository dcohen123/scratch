window.onload=function(){
	var imageForm = document.getElementById("imageForm");
	var imageText = document.getElementById("imageText");
	var imageFile = document.getElementById("imageFile");
	imageForm.addEventListener("submit", function(e){
		e.preventDefault();
		var reader = new FileReader();
		reader.onload = function(){
			var img = new Image();
			img.onload = function(){
				OCRAD(img, function(text){
					imageText.innerHTML = text;
					var msg = new SpeechSynthesisUtterance(text);
					window.speechSynthesis.speak(msg); 	
				});
			}
			img.src = reader.result;
		}
		reader.readAsDataURL(imageFile.files[0]);
	});
}